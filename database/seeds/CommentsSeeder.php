<?php

use Faker\Factory as Faker;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        //'content', 'post_id', 'user_id'
        foreach (range(1, 200) as $index) {
            $input['content'] = $faker->realText(100, 5);
            //$input['date'] = \Carbon::now();
            $input['post_id'] = \App\Post::inRandomOrder()->first()->id;
            $input['user_id'] = \App\User::inRandomOrder()->first()->id;
            try {
                \App\Comment::create($input);
            } catch (\Exception $e) { }
        }
    }
}
