<?php
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class LikesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //'isLike', 'user_id', 'comment_id'
        $faker = Faker::create();
        //'content', 'post_id', 'user_id'
        foreach (range(1, 200) as $index) {
            $input['isLike'] = $faker->boolean();
            //$input['date'] = \Carbon::now();
            $input['post_id'] = \App\Post::inRandomOrder()->first()->id;
            $input['user_id'] = \App\User::inRandomOrder()->first()->id;
            try {
                \App\Comment::create($input);
            } catch (\Exception $e) { }
        }
    }
}
