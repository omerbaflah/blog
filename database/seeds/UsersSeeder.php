<?php

use App\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();


        foreach (range(1, 25) as $index) {

            $input ['name'] = $faker->name;
            $input ['username'] = substr($faker->userName, 0, 16);
            $input ['email'] = $faker->freeEmail;
            $input ['password'] = bcrypt($faker->password);

            if (User::count() == 0) {
                $input ['username'] = 'admin';
                $input ['password'] = bcrypt('admin');
                $input ['email'] = 'admin@admin.com';
            }

            try {

              $user = User::create($input);

                if ($user->username == 'admin' && $user->email == 'admin@admin.com') {
                    $user->assignRole('Admin');
                }else{
                    $user->assignRole('User');
                }

            } catch (\Exception $e) {

            }

            
            
        }
    }
}
