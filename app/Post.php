<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    //php artisan crud:api Comments --fields='Id#bigint;content#text;date#datetime;post_id#bigint#unsigned;user_id#bigint#unsigned' --controller-namespace=API\v1 --soft-deletes

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'image', 'user_id'];

    protected $hidden = ['deleted_at'];

    //protected $with = ['comments'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'body' => 'min:5'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public  function stars()
    {

        return $this->hasMany(Star::class);
    }
}
