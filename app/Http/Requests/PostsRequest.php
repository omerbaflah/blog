<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class PostsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod()) {
                // handle creates
            case 'post':
            case 'POST':
                return [
                    'title' => 'required|min:3',
                    'description' => 'required|min:20',
                    'image' => 'required|image',
                    //'user_id' => 'required|exists:users,id',

                ];

                // Handle updates
            case 'patch':
            case 'PATCH':
                return [
                    'title' => 'min:3',
                    'description' => 'min:20',
                    'image' => 'image',
                    //'user_id' => 'exists:users,id',

                ];

                // handle deletions
            case 'delete':
            case 'DELETE':
                return [
                    'ids' => 'required',
                    'ids.*' => 'required|integer'
                ];
        }
        // return empty array for other requests
        return [
            //
        ];
    }
    protected function failedValidation(Validator $validator)
    {

        throw new HttpResponseException(
            sendError($validator->errors()->first(), null, 422)

        );
    }
}
