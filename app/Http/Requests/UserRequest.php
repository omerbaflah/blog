<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserRequest extends FormRequest
{
    //'name', 'username',  'email', 'password',
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod()) {
                // handle creates
            case 'post':
            case 'POST':
                return [
                    'name' => 'required|min:3',
                    'username' => 'required|min:3',
                    'email' => 'required|email|unique:users',
                    'password' => 'required|min:3',

                ];

                // Handle updates
            case 'patch':
            case 'PATCH':
                return [
                    'name' => 'min:3',
                    'username' => 'min:3',
                    'email' => 'email|unique:users',
                    'password' => 'min:3',

                ];

                // handle deletions
            case 'delete':
            case 'DELETE':
                return [
                    'ids' => 'required',
                    'ids.*' => 'required|integer'
                ];
        }
        // return empty array for other requests
        return [
            //
        ];
    }
    protected function failedValidation(Validator $validator)
    {

        throw new HttpResponseException(
            sendError($validator->errors()->first(), null, 422)

        );
    }
}
