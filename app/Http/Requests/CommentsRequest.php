<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class CommentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod()) {
                // handle creates
            case 'post':
            case 'POST':
                return [
                    'content' => 'required|min:3',
                    //'state' => 'required', Rule::in([true, 'true', 'on', 'yes', 1, false, 'false', 'off', 'no', 0]),
                    'post_id' => 'required|exists:posts,id',
                    //'user_id' => 'required|exists:users,id',

                ];

                // Handle updates
            case 'patch':
            case 'PATCH':
                return [
                    'content' => 'min:3',
                    'state' => Rule::in([true, 'true', 'on', 'yes', 1, false, 'false', 'off', 'no', 0,null]),
                    'post_id' => 'exists:posts,id',
                    //'user_id' => 'exists:users,id',
                ];

                // handle deletions
            case 'delete':
            case 'DELETE':
                return [
                    'ids' => 'required',
                    'ids.*' => 'required|integer'
                ];
        }

        return [
            //
        ];
    }
    protected function failedValidation(Validator $validator)
    {

        throw new HttpResponseException(
            sendError($validator->errors()->first(), null, 422)

        );
    }
}
