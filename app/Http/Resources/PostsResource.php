<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        //created_at
        return [

            'id'=>$this->id,
            'title'=>$this->title,
            'description'=>Str::limit($this->description, 20, ' ...'),
            'image'=>Storage::disk()->url($this->image),
            'user'=> UserResource::make( $this->user()->first()),
            'created_at'=>$this->created_at,
        ];    }
}
