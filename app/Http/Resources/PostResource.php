<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'id' => $this->id,
            'title' => $this->title,
            'description' => Str::limit($this->description, 20, ' ...'),
            'image' => Storage::disk()->url($this->image),
            'user_id' => $this->user_id,
            'comments' => $this->comments()->select(['id', 'content'])->get()
        ];
    }
}
