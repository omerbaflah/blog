<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class PostsListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //'title', 'description', 'image', 'user_id'
        return [

            'id' => $this->id,
            'title' => Str::limit($this->title, 20, ' ...'),

        ];
    }
}
