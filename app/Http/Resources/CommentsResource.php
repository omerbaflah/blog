<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class CommentsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'id' => $this->id,
            //'content' => substr($this->content, 0, 20),
            'content' => Str::limit($this->content, 20, ' ...'),
            'state' => $this->state,
            'post' => $this->post()->select(['id', 'title'])->first(),
            'user' => $this->user()->select(['id', 'username'])->first(),
        ];
    }
}
