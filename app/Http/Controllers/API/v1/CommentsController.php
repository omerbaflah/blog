<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Requests;
//use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Comment;
use App\Http\Requests\CommentsRequest;
use App\Http\Resources\CommentResource;
use App\Http\Resources\CommentsListResource;
use App\Http\Resources\CommentsResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        unsetEmptyParams($request);

        if ($request->has('list') && $request->list == true) {
            $comments = CommentsListResource::collection(Comment::all());

            return sendResponse(__('messages.get_data'), ['comments' => $comments]);
        }

        if ($request->has('per_page')) {
            $per_page = $request->per_page;
        }

        $comments = Comment::query();


        //$post = \App\Comment::onlyTrashed()->get();

        //return [$post];
        if ($request->has('search_text')) {
            $comments->where('', 'like', "%{$request->get('search_text')}%");
        }

        $sort = $request->sort ?? 'asc';
        $orderBy = $request->orderBy ?? 'id';

        $comments = CommentsResource::collection($comments->orderBy($orderBy, $sort)->paginate($per_page ?? 10))->appends($request->query())->toArray();

        $comments_array = $comments['data'];
        unset($comments['data']);
        $comments['comments'] = $comments_array;

        return sendResponse(__('messages.get_data'), $comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CommentsRequest $request)
    {
        unsetEmptyParams($request);
        $input = $request->only((new Comment())->getFillable());

        if (Auth::check()) {
            $input['user_id'] = Auth::user()->id;
        }
        $input['state'] = true;
        $comment = Comment::create($input);

        return sendResponse(__('messages.create_data'), CommentResource::make($comment));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::findOrFail($id);
        //return $comment;
        return sendResponse(__('messages.get_data'), CommentResource::make($comment));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CommentsRequest $request, $id)
    {
        unsetEmptyParams($request);

        $input = $request->only((new Comment())->getFillable());
        if ($request->has('state')) {
            $input['state'] = \filter_var($request->state, FILTER_VALIDATE_BOOLEAN);
        }
        if (Auth::check()) {
            $input['user_id'] = Auth::user()->id;
        }

        $comment = Comment::findOrFail($id);
        $comment->update($input);

        return sendResponse(__('messages.update_data'), CommentResource::make($comment));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommentsRequest $request)
    {
        Comment::destroy($request->ids);

        return sendResponse(__('messages.delete_data'), null, 200);
    }
}
