<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Like;
use Illuminate\Http\Request;

class LikesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        unsetEmptyParams($request);

        if ($request->has('list') && $request->list == true) {
            $likes = LikeListResource::collection(Like::all());

            return sendResponse(__('messages.get_data'), ['likes' => $likes]);
        }

        if ($request->has('per_page')) {
            $per_page = $request->per_page;
        }

        $likes = Like::query();

        if ($request->has('search_text')) {
            $likes->where('', 'like',"%{$request->get('search_text')}%");
        }

        $sort = $request->sort ?? 'asc';
        $orderBy = $request->orderBy ?? 'id';

        $likes = LikeResource::collection($likes->orderBy($orderBy, $sort)->paginate($per_page ?? 10))->appends($request->query())->toArray();

         $likes_array = $likes['data'];
         unset($likes['data']);
         $likes['likes'] = $likes_array;

         return sendResponse(__('messages.get_data'), $likes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\LikeRequest $request)
    {
        unsetEmptyParams($request);
        $input = $request->only((new Like())->getFillable());

        $like = Like::create($input);

        return sendResponse(__('messages.create_data'), LikeResource::make($like));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $like = Like::findOrFail($id);

        return sendResponse(__('messages.get_data'), LikeResource::make($like));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\LikeRequest $request, $id)
    {
        unsetEmptyParams($request);

        $input = $request->only((new Like())->getFillable());

        $like = Like::findOrFail($id);
        $like->update($input);

        return sendResponse(__('messages.update_data'), LikeResource::make($like));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requests\LikeRequest $request)
    {
        Like::destroy($request->ids);

        return sendResponse(__('messages.delete_data'), null, 200);
    }
}
