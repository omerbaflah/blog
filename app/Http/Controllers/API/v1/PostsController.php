<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Resources\PostResource;
use App\Http\Resources\PostsListResource;
use App\Http\Resources\PostsResource;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        unsetEmptyParams($request);

        if ($request->has('list') && $request->list == true) {
            $posts = PostsListResource::collection(Post::all());

            return sendResponse(__('messages.get_data'), ['posts' => $posts]);
        }

        if ($request->has('per_page')) {
            $per_page = $request->per_page;
        }

        $posts = Post::query();

        if ($request->has('search_text')) {
            $posts->where('title', 'like',"%{$request->get('search_text')}%");
        }

        $sort = $request->sort ?? 'asc';
        $orderBy = $request->orderBy ?? 'id';

        $posts = PostsResource::collection($posts->orderBy($orderBy, $sort)->paginate($per_page ?? 10))->appends($request->query())->toArray();

         $posts_array = $posts['data'];
         unset($posts['data']);
         $posts['posts'] = $posts_array;

         return sendResponse(__('messages.get_data'), $posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PostsRequest $request)
    {
        unsetEmptyParams($request);
        $input = $request->only((new Post())->getFillable());


        if ($request->hasFile('image')) {
            $image_path = $request->file('image')->store('posts');
            $input ['image'] = $image_path;
        }

        
        if(Auth::check()){
            $input['user_id'] = Auth::user()->id;
        }

        $post = Post::create($input);

        return sendResponse(__('messages.create_data'), PostResource::make($post));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);

        return sendResponse(__('messages.get_data'), PostResource::make($post));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\PostsRequest $request, $id)
    {
        unsetEmptyParams($request);

        $input = $request->only((new Post())->getFillable());
        if ($request->hasFile('image')) {
            $image_path = $request->file('image')->store('posts');
            $input ['image'] = $image_path;
        }

        if(Auth::check()){
            $input['user_id'] = Auth::user()->id;
        }


        $post = Post::findOrFail($id);
        $post->update($input);

        return sendResponse(__('messages.update_data'), PostResource::make($post));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requests\PostsRequest $request)
    {
        Post::destroy($request->ids);

        return sendResponse(__('messages.delete_data'), null, 200);
    }
}
