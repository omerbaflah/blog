<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Resources\UserListResource;
use App\Http\Resources\UsersResource;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        unsetEmptyParams($request);

        if ($request->has('list') && $request->list == true) {

            $users = UserListResource::collection(User::all());

            return sendResponse(__('messages.get_data'), ['users' => $users]);
        }

        if ($request->has('per_page')) {
            $per_page = $request->per_page;
        }

        $users = User::query();

        if ($request->has('search_text')) {
            $users->where('name', 'like', "%{$request->get('search_text')}%");
        }

        $sort = $request->sort ?? 'asc';
        $orderBy = $request->orderBy ?? 'id';   

        $users = UsersResource::collection($users->orderBy($orderBy, $sort)->paginate($per_page ?? 10))->appends($request->query())->toArray();



        $users_array = $users['data'];
        unset($users['data']);
        $users['users'] = $users_array;

        return sendResponse(__('messages.get_data'), $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        unsetEmptyParams($request);
        $input = $request->only((new User())->getFillable());
        $input['password'] = bcrypt($input['password']);

        //bcrypt($input('password'));

        $user = User::create($input);

        if ($request->has('role') && $request->input('role') != 'Admin') {
            $user->assignRole($request->input('role'));
        }

        return sendResponse(__('messages.create_data'), UserResource::make($user));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return sendResponse(__('messages.get_data'), UserResource::make($user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        unsetEmptyParams($request);

        $input = $request->only((new User())->getFillable());

        $user = User::findOrFail($id);

        if ($input['password'] != '') {
            $input['password'] = bcrypt($input['password']);
        }

        if ($request->has('role') && $request->input('role') != '') {
            if($request->input('role') != 'Admin'){
                $user->syncRoles($request->input('role'));
            }else{
                $user->syncRoles($request->input('role'));
            }
        }

        $user->update($input);

        return sendResponse(__('messages.update_data'), UserResource::make($user));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserRequest $request)
    {
        User::destroy($request->ids);

        return sendResponse(__('messages.delete_data'), null, 200);
    }
}
