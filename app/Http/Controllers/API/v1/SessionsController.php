<?php

namespace App\Http\Controllers\API\v1;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SessionsController
{

    /**
     * User login api => username and password required
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'login' => 'required',
            //'email' => 'email',
            'password' => 'required|min:5'
        ]);

        if ($validator->fails()) {
            return sendError($validator->errors()->first(), null, 401);
        }

        if (\filter_var(request('login'), FILTER_VALIDATE_EMAIL)) {
            Auth::attempt(['email' => request('login'), 'password' => request('password')]);
        } else {
            Auth::attempt(['username' => request('login'), 'password' => request('password')]);
        }

        if (Auth::check()) {

            $user = Auth::user();
            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['name'] = $user->name;
            $success['id'] = $user->id;

            return sendResponse(__('auth.success_login'), $success);
        }


        /*if (Auth::attempt(['email' => request('email'), 'username' => request('username'), 'password' => request('password')])) {

            $user = Auth::user();

            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['name'] = $user->name;
            $success['id'] = $user->id;

            return sendResponse(__('auth.success_login'), $success);
        }*/

        return sendError(__('auth.failed'), null, 401);
    }

    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {

        unsetEmptyParams($request);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email|unique:users',
            //'password' => 'required|string|confirmed'
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            return sendError($validator->errors()->first(), null, 401);
        }

        $input = $request->only((new User())->getFillable());

        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);

        $user->assignRole('User');

        //$user->createToken('MyApp')-> accessToken; 
        /*$user = new User([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        $user->save();*/

        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->username;

        return sendResponse(__('auth.success_register'), $success);
    }

    /**
     * User logout api => accessToken  required
     *
     * @return 204
     */

    public function logout()
    {
        $token = auth()->guard('api')->user()->token();
        $token->revoke();

        return sendResponse(__('auth.logout'), null, 204);
    }
}


/*if (Auth::attempt(['email' => request('email'), 'username' => request('username'), 'password' => request('password')])) {

            $user = Auth::user();

            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['name'] = $user->name;
            $success['id'] = $user->id;

            return sendResponse(__('auth.success_login'), $success);
        }*/
