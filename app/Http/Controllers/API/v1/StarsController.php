<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Star;
use Illuminate\Http\Request;

class StarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        unsetEmptyParams($request);

        if ($request->has('list') && $request->list == true) {
            $stars = StarListResource::collection(Star::all());

            return sendResponse(__('messages.get_data'), ['stars' => $stars]);
        }

        if ($request->has('per_page')) {
            $per_page = $request->per_page;
        }

        $stars = Star::query();

        if ($request->has('search_text')) {
            $stars->where('', 'like',"%{$request->get('search_text')}%");
        }

        $sort = $request->sort ?? 'asc';
        $orderBy = $request->orderBy ?? 'id';

        $stars = StarResource::collection($stars->orderBy($orderBy, $sort)->paginate($per_page ?? 10))->appends($request->query())->toArray();

         $stars_array = $stars['data'];
         unset($stars['data']);
         $stars['stars'] = $stars_array;

         return sendResponse(__('messages.get_data'), $stars);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StarRequest $request)
    {
        unsetEmptyParams($request);
        $input = $request->only((new Star())->getFillable());

        $star = Star::create($input);

        return sendResponse(__('messages.create_data'), StarResource::make($star));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $star = Star::findOrFail($id);

        return sendResponse(__('messages.get_data'), StarResource::make($star));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\StarRequest $request, $id)
    {
        unsetEmptyParams($request);

        $input = $request->only((new Star())->getFillable());

        $star = Star::findOrFail($id);
        $star->update($input);

        return sendResponse(__('messages.update_data'), StarResource::make($star));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requests\StarRequest $request)
    {
        Star::destroy($request->ids);

        return sendResponse(__('messages.delete_data'), null, 200);
    }
}
