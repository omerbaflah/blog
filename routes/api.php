<?php

use App\Http\Controllers\API\v1\SessionsController;

Route::post('login', [SessionsController::class, 'login']);
Route::post('signup', [SessionsController::class, 'signup']);

/**
 * Here we put protected routes
 */


//Route::get('x', [\App\Http\Controllers\A::class, 'x']);

Route::middleware('auth:api')->group(function () {
    Route::post('logout', [SessionsController::class, 'logout']);

    Route::apiResource('users', \App\Http\Controllers\API\v1\UsersController::class)->except(['create', 'edit']);
    Route::delete('users', [\App\Http\Controllers\API\v1\UsersController::class, 'destroy']);

    Route::apiResource('posts', \App\Http\Controllers\API\v1\PostsController::class)->except(['create', 'edit']);
    Route::delete('posts', [\App\Http\Controllers\API\v1\PostsController::class, 'destroy']);

    Route::apiResource('comments', \App\Http\Controllers\API\v1\CommentsController::class)->except(['create', 'edit']);
    Route::delete('comments', [\App\Http\Controllers\API\v1\CommentsController::class, 'destroy']);
});

Route::group(['middleware' => ['role:Admin']], function () {
    //
});

Route::group(['middleware' => ['role:User']], function () {
    //
});
Route::get('posts', [\App\Http\Controllers\API\v1\PostsController::class, 'index']);
Route::get('posts/{post}', [\App\Http\Controllers\API\v1\PostsController::class, 'show']);


Route::get('test', [\App\Http\Controllers\API\v1\Test::class, 'index']);





//Route::get('comments/deleted', [\App\Http\Controllers\API\v1\CommentsController::class, 'DeletedComments']);

Route::apiResource('stars', \App\Http\Controllers\API\v1\StarsController::class)->except(['create', 'edit']);
Route::delete('stars', [\App\Http\Controllers\API\v1\StarsController::class, 'destroy']);

Route::apiResource('likes', \App\Http\Controllers\API\v1\LikesController::class)->except(['create', 'edit']);
Route::delete('likes', [\App\Http\Controllers\API\v1\LikesController::class, 'destroy']);
