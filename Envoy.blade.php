@servers(['web' => 'ubuntu@34.212.164.242'])

@setup
$repository = ' git@gitlab.com:fh32000/laundry_app.git';
$releases_dir = '/var/www/html/staging_laundry_app';
$app_dir = '/var/www/html/staging_laundry_app';
$release = date('YmdHis');
$new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
clone_repository
run_composer
update_symlinks
setup_app
@endstory

@task('clone_repository')
echo 'Cloning repository'
[ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
cd {{ $new_release_dir }}
git reset --hard {{ $commit }}
@endtask

@task('run_composer')
echo "Starting deployment ({{ $release }})"
cd {{ $new_release_dir }}
composer install --quiet --prefer-dist --optimize-autoloader
composer dumpautoload
@endtask

@task('update_symlinks')
echo "Linking storage directory"
rm -rf {{ $new_release_dir }}/storage
ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

echo 'Linking .env file'
ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

echo 'Linking current release'
ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('setup_app')
echo "Setting up the app"
cd {{ $new_release_dir }}
php artisan storage:link
php artisan key:generate
php artisan migrate --force
php artisan cache:clear
php artisan config:clear
php artisan config:cache
php artisan passport:install
@endtask
