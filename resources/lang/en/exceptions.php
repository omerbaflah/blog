<?php

return [
    'record_not_found' => 'Record not found',
    'bad_request' => 'Bad request',
    'login_required' => 'You have to login',
    'forbidden' => 'You are unauthorized for this action'

];