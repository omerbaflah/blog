<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials are incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'success_register' => 'you have registered successfully',
    'logout' => 'logged out successfully.',
    'not_authorized' => 'You are not authorized to do this action.',
    'success_login' => 'you have logged in successfully',
    'verify_email' => 'You have to verify your email.',
    'invalid_token' => 'This activation token is invalid.',
    'email_verified' => 'Your email has been verified successfully.'
];
